# 5 задание
### Что такое git bash?

Git Bash — это командная строка (терминал) для работы с системой контроля версий Git на операционных системах Windows. Она предоставляет среду командной строки, основанную на оболочке Bash (Bourne Again SHell), в которой можно выполнять Git-команды и взаимодействовать с репозиториями Git.

Git Bash предоставляет мощный инструментарий для работы с Git, позволяя выполнять команды, такие как инициализация репозитория, создание коммитов, ветвление, слияние, переключение между ветками, управление удалёнными репозиториями и многое другое. Благодаря использованию Bash в Git Bash, вы также можете выполнять команды и скрипты, принятые в Unix-подобных системах, что делает его более гибким и удобным для разработчиков, привыкших к работе с командной строкой в Linux или macOS.

### Что делает команда `git pull`? Чем она отличается от `git push`?

Команда git pull и команда git push являются двумя разными командами в системе контроля версий Git и выполняют различные операции:

Команда git pull используется для получения (получения и объединения) изменений из удаленного репозитория Git в текущую ветку вашего локального репозитория. Она включает две основные операции: git fetch и git merge. Команда git fetch получает все изменения из удаленного репозитория, а команда git merge объединяет полученные изменения с текущей веткой. Таким образом, git pull позволяет вам обновить ваш локальный репозиторий до последних изменений, внесенных в удаленный репозиторий.

Команда git push используется для отправки (записи и передачи) изменений из вашего локального репозитория в удаленный репозиторий Git. Когда вы делаете коммиты и вносите изменения в своем локальном репозитории, команда git push позволяет вам отправить эти изменения в удаленный репозиторий, чтобы они стали доступными для других участников проекта.

Таким образом, основное отличие между git pull и git push заключается в направлении передачи изменений: git pull получает изменения из удаленного репозитория в ваш локальный репозиторий, а git push отправляет изменения из вашего локального репозитория в удаленный репозиторий.

### Что такое merge request?

Merge Request (MR), также известный как Pull Request (PR), это механизм, используемый в системах контроля версий для предложения и обсуждения изменений, внесенных в кодовую базу. Основная цель MR/PR - это предоставление возможности разработчикам вносить изменения в проект и предлагать их для объединения с основной веткой (обычно называемой "веткой мастера" или "веткой разработки").

MR/PR предоставляет удобный механизм для сотрудничества, обсуждения и рецензирования изменений, а также помогает поддерживать хорошую контрольную точку перед объединением кода в основную ветку проекта.

### Чем между собой отличаются команды `git status` и `git log`?

Команды git status и git log в Git выполняют разные функции и предоставляют различную информацию о состоянии репозитория и истории коммитов:

Команда git status позволяет получить информацию о текущем состоянии вашего локального репозитория. Она показывает, какие файлы были изменены, добавлены или удалены, но ещё не внесены в коммит. git status также отображает текущую ветку и информацию о ветке, на которую вы находитесь, а также информацию о том, есть ли внесенные изменения, которые еще не были отправлены на удаленный репозиторий. Команда git status полезна для отслеживания состояния вашей рабочей области и предоставляет контекст для принятия решений о дальнейшей работе.

Команда git log позволяет просматривать историю коммитов в репозитории. Она показывает список коммитов в хронологическом порядке, начиная с самого последнего коммита. Каждый коммит включает информацию, такую как автор коммита, дата и время коммита, сообщение коммита и хеш коммита. git log также может отображать различные опции форматирования и фильтрации для более подробного просмотра истории коммитов. Команда git log полезна для изучения истории изменений, отслеживания авторства и анализа коммитов в репозитории.

Таким образом, git status предоставляет информацию о текущем состоянии вашего репозитория и рабочей области, в то время как git log позволяет просматривать историю коммитов и получать информацию о каждом коммите в репозитории.

### Что такое submodule? С помощью какой команды можно добавлять сабмодули в свой репозиторий?

Submodule (сабмодуль) в Git - это ссылка на другой репозиторий Git, который может быть включен как поддиректория внутри основного репозитория. Это позволяет вам включать в свой проект внешние зависимости или использовать код из других репозиториев без необходимости копирования его содержимого в основной репозиторий.

Для добавления сабмодуля в свой репозиторий в Git используется команда git submodule add.
